import sys
from scapy.all import *
from dns import resolver
from pprint import pprint

res = resolver.Resolver()
res.nameservers = ['1.1.1.1']

key = {
    "T" : "duckduckgo.com",
    "E" : "archlinux.org",
    "S" : "privacytools.io",
    "T" : "element.io"
}

key_reverse = {}

for i in key:
    response = res.resolve(key[i])
    for rdata in response:
        key_reverse[rdata.address] = i

def decode(pkt):
    try:
        rdata_ip = pkt[DNS].an[0].rdata
        print(key_reverse[rdata_ip])
    except:
        print("Not in key")

recv_pkt = sniff(filter="ip and port 11333",lfilter=lambda x:x.haslayer(DNS) ,prn=decode, iface='wlp0s20f0u9')
    
