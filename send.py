import sys
from scapy.all import *
from dns import resolver
from pprint import pprint
import time

res = resolver.Resolver()
res.nameservers = ['1.1.1.1']

key = {
    "T" : "duckduckgo.com",
    "E" : "archlinux.org",
    "S" : "privacytools.io",
    "T" : "element.io",
    "P" : "mozilla.org"
}

key_reverse = {}

for i in key:
    key_reverse[res.resolve(key[i])[0].address] = i

message = "TEST"

for i in message:
    send_pkt=sendp(IP(id=random.randint(0, 0xFFFF),dst="1.1.1.1",src="192.168.0.201")/UDP(dport=53,sport=53)/DNS(id=random.randint(0, 0xFFFF),rd=1,qd=DNSQR(qname=key[i])))
    time.sleep(1)
